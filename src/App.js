import { Suspense, useLayoutEffect, useMemo } from 'react';
import { Canvas, useLoader, useThree } from '@react-three/fiber';
import { OrbitControls } from '@react-three/drei';
import { ShaderMaterial, TextureLoader, UniformsUtils } from 'three';
import { useControls } from 'leva';


import { ParallaxShader } from './POM.js';

import './App.css';


function ParallaxMaterial(maps) {

    const shader = ParallaxShader;
    const uniforms = UniformsUtils.clone(shader.uniforms);

    const { parallaxScale } = useControls({
        parallaxScale: {
            value: 0.25,
            min: 0,
            max: 1,
            step: 0.05
        }
    });
    const { parallaxMinLayers } = useControls({
        parallaxMinLayers: {
            value: 25,
            min: 1,
            max: 30,
            step: 1
        }
    });
    const { parallaxMaxLayers } = useControls({
        parallaxMaxLayers: {
            value: 30,
            min: 2,
            max: 30,
            step: 1
        }
    });

    const parameters = {
        fragmentShader: shader.fragmentShader,
        vertexShader: shader.vertexShader,
        uniforms: uniforms
    }

    const material = useMemo(() => new ShaderMaterial(parameters), 
        [parameters]
    );

    material.map = maps.colorMap;
    material.map.anisotropy = 4;

    material.bumpMap = maps.bumpMap;
    material.bumpMap.anisotropy = 4;

    uniforms.map.value = material.map;
    uniforms.bumpMap.value = material.bumpMap;

    uniforms.parallaxScale.value = 1.0 * parallaxScale;

    uniforms.parallaxMinLayers.value = parallaxMinLayers;
    uniforms.parallaxMaxLayers.value = parallaxMaxLayers;

    return material;
}

function ParallaxPlane() {

    const colorMap = useLoader(TextureLoader, 'color.png');
    const bumpMap = useLoader(TextureLoader, 'bump2.png');

    const camera = useThree((state) => state.camera);

    const material = ParallaxMaterial({
        colorMap: colorMap,
        bumpMap: bumpMap,
    });


    useLayoutEffect(() => {
        camera.position.z = 1;
    }, []);


    return (
        <mesh material={material}>
            <planeBufferGeometry />
        </mesh>
    )

}


function App() {

    return (
        <div className="app">
            <Canvas gl={{antialias: true}} dpr={window.devicePixelRatio}>
                <Suspense fallback={null}>
                    <ParallaxPlane />
                </Suspense>
                <OrbitControls />
            </Canvas>
        </div>
    );
}

export default App;