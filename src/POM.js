// Parallax Occlusion shaders from
//    http://sunandblackcat.com/tipFullView.php?topicid=28
// No tangent-space transforms logic based on
//   http://mmikkelsen3d.blogspot.sk/2012/02/parallaxpoc-mapping-and-no-tangent.html

const ParallaxShader = {

  uniforms: {
    'bumpMap': { value: null },
    'map': { value: null },
    'parallaxScale': { value: null },
    'parallaxMinLayers': { value: null },
    'parallaxMaxLayers': { value: null }
  },

  vertexShader: /* glsl */`

    varying vec2 vUv;
    varying vec3 vViewPosition;
    varying vec3 vNormal;

    void main() {

      vUv = uv;
      vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );
      vViewPosition = -mvPosition.xyz;
      vNormal = normalize( normalMatrix * normal );
      gl_Position = projectionMatrix * mvPosition;

    }`,

  fragmentShader: /* glsl */`

    uniform sampler2D bumpMap;
    uniform sampler2D map;

    uniform float parallaxScale;
    uniform float parallaxMinLayers;
    uniform float parallaxMaxLayers;

    varying vec2 vUv;
    varying vec3 vViewPosition;
    varying vec3 vNormal;


      vec2 parallaxMap( in vec3 V ) {

        // Determine number of layers from angle between V and N
        float numLayers = mix( parallaxMaxLayers, parallaxMinLayers, abs( dot( vec3( 0.0, 0.0, 1.0 ), V ) ) );

        float layerHeight = 1.0 / numLayers;
        float currentLayerHeight = 0.0;
        // Shift of texture coordinates for each iteration
        vec2 dtex = parallaxScale * V.xy / V.z / numLayers;

        vec2 currentTextureCoords = vUv;

        float heightFromTexture = texture2D( bumpMap, currentTextureCoords ).r;

        // while ( heightFromTexture > currentLayerHeight )
        // Infinite loops are not well supported. Do a "large" finite
        // loop, but not too large, as it slows down some compilers.
        for ( int i = 0; i < 30; i += 1 ) {
          if ( heightFromTexture <= currentLayerHeight ) {
            break;
          }
          currentLayerHeight += layerHeight;
          // Shift texture coordinates along vector V
          currentTextureCoords -= dtex;
          heightFromTexture = texture2D( bumpMap, currentTextureCoords ).r;
        }


      vec2 prevTCoords = currentTextureCoords + dtex;

        // Heights for linear interpolation
        float nextH = heightFromTexture - currentLayerHeight;
        float prevH = texture2D( bumpMap, prevTCoords ).r - currentLayerHeight + layerHeight;

        // Proportions for linear interpolation
        float weight = nextH / ( nextH - prevH );

        // Interpolation of texture coordinates
        return prevTCoords * weight + currentTextureCoords * ( 1.0 - weight );
      }


    vec2 perturbUv( vec3 surfPosition, vec3 surfNormal, vec3 viewPosition ) {

      vec2 texDx = dFdx( vUv );
      vec2 texDy = dFdy( vUv );

      vec3 vSigmaX = dFdx( surfPosition );
      vec3 vSigmaY = dFdy( surfPosition );
      vec3 vR1 = cross( vSigmaY, surfNormal );
      vec3 vR2 = cross( surfNormal, vSigmaX );
      float fDet = dot( vSigmaX, vR1 );

      vec2 vProjVscr = ( 1.0 / fDet ) * vec2( dot( vR1, viewPosition ), dot( vR2, viewPosition ) );
      vec3 vProjVtex;
      vProjVtex.xy = -1.0 * (texDx * vProjVscr.x + texDy * vProjVscr.y);
      vProjVtex.z = -1.0 * dot( surfNormal, viewPosition );

      return parallaxMap( vProjVtex );
    }

    void main() {

      vec2 mapUv = perturbUv( -vViewPosition, normalize( vNormal ), normalize( vViewPosition ) );

      gl_FragColor = texture2D( map, mapUv );

    }`

};

export { ParallaxShader };